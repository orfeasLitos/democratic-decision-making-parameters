import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import math
import json
import operator

with open("probs.json", "r") as f:
    probs = np.array(json.load(f))

with open("parameters.json", "r") as f:
    # ss in proportions of P, ns in proportions of P, ts in proportions of n
    P, ss, ns, ts = operator.itemgetter("P", "ss", "ns", "ts")(json.load(f))

fig = plt.figure(constrained_layout=True)
grid_side = math.ceil(math.sqrt(len(ns)))
spec = gridspec.GridSpec(ncols=grid_side, nrows=grid_side, figure=fig, hspace=0.2)

yticks = np.arange(0, math.floor(P*ss[0]) + 1, math.floor(P*ss[0]/2))

for i, n_prop in enumerate(ns):
    n = math.floor(n_prop*P)

    axes = fig.add_subplot(spec[i//grid_side, i%grid_side], projection="3d")
    axes.set_title(f"n = {n}")
    axes.set_xlabel("t")
    axes.set_ylabel("s")
    axes.set_zlabel("Pr[fail]")
    axes.set_zlim(0, 1)

    xmax = n*ts[0]
    xticks = [math.floor(x) for x in [0, xmax/2, xmax]]
    axes.set_xticks(xticks)
    axes.set_yticks(yticks)

    xs, ys = np.meshgrid([math.floor(n*t) for t in ts], [math.floor(P*s) for s in ss])
    xs, ys = xs.ravel(), ys.ravel()
    zs = probs[i, :, :].ravel()
    axes.plot_trisurf(xs, ys, zs)

plt.show()
