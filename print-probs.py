from math import ceil
from badevent import p_badevent

P = 10000
B = 11
while True:
    P = P+10
    n = int(P/100)
    s = int(P/4)
    t = int(n/2)
    be = p_badevent(P, n, s, t)
    print(P, be*ceil(P/B))
